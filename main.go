package main

import (
	"log"
	"net/http"
)

func redirectToPath(w http.ResponseWriter, r *http.Request) {

	//get the hostname
	host := r.Host

	switch host {
	case "broker.cocowork.co":
		log.Println("redirecting broker.cocowork.co ")
		http.Redirect(w, r, "https://a.pgtb.me/HcK27N", http.StatusMovedPermanently)
	case "refer.cocowork.co":
		log.Println("redirecting refer.cocowork.co ")
		http.Redirect(w, r, "https://a.pgtb.me/mxNq62", http.StatusMovedPermanently)
	default:
		log.Println("executing default case ")
		http.Redirect(w, r, "https://www.cocowork.co", http.StatusMovedPermanently)
	}

}

func main() {
	http.HandleFunc("/", redirectToPath) // set router

	log.Println("Listening On Port 9090")

	err := http.ListenAndServe(":9090", nil) // set listen port

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
